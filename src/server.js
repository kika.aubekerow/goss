const http = require("http");

const server = http.createServer((req, res) => {
  const urlPath = req.url;
  
  if (urlPath === "/overview") {
    res.end('Welcome to the "overview page" of the nginX project');
  } else if (urlPath === "/api") {
    res.writeHead(200, { "Content-Type": "application/json" });
    res.end(
      JSON.stringify({
        product_id: "xyz12u3",
        product_name: "NginX injector",
      })
    );
  } else {
   res.setHeader("X-Author", "itmo250410")
   res.setHeader("Access-Control-Allow-Origin", "*")
   res.end("itmo250410");
  }
})

server.listen(3000, "localhost", () => {
  console.log("Listening for request");
});
